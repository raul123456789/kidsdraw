package com.raul.kidsdrawingapp

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBHelper(context: Context, factory: SQLiteDatabase.CursorFactory?) :
                SQLiteOpenHelper(context, Constants.DATABASE_NAME, factory, Constants.DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase?) {
        val query = ("CREATE TABLE " + Constants.TABLE_NAME + " ("
                + Constants.ID_COL + " INTEGER PRIMARY KEY, "
                + Constants.IMAGE_NAME + " TEXT," + ")")
        db!!.execSQL(query)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    fun addImage(imgPathName : String) {
        val values = ContentValues()
        values.put(Constants.IMAGE_NAME, imgPathName)
        val db = this.writableDatabase
        db.insert(Constants.TABLE_NAME, null, values)
        db.close()
    }

    fun getImage() {

    }


}