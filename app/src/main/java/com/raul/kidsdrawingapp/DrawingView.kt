package com.raul.kidsdrawingapp

import android.content.Context
import android.graphics.*
import android.os.Build
import android.util.AttributeSet
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import androidx.annotation.RequiresApi
import kotlin.math.cos
import kotlin.math.sin


@RequiresApi(Build.VERSION_CODES.Q)
class DrawingView(context: Context, attrs: AttributeSet) : View(context, attrs) {

    private var mDrawPath : CustomPath? = null
    private var mCanvasBitmap: Bitmap? = null
    private var mDrawPaint : Paint? = null
    private var mCanvasPaint : Paint? = null
    private var mBrushSize: Float=0.toFloat()
    private var color = Color.BLACK
    private var canvas: Canvas?=null
    private val mPaths=ArrayList<CustomPath>()
    private val mUndoPaths=ArrayList<CustomPath>()

    init {
        setupDrawing()
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun setupDrawing() {
        mDrawPaint = Paint()
        mDrawPath  = CustomPath(color, mBrushSize)
        mDrawPaint!!.color=color
        mDrawPaint!!.style=Paint.Style.STROKE
        mDrawPaint!!.strokeJoin=Paint.Join.MITER
        mDrawPaint!!.strokeCap=Paint.Cap.ROUND
        mCanvasPaint=Paint(Paint.DITHER_FLAG)
        //mBrushSize=25.toFloat()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        mCanvasBitmap=Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
        canvas = Canvas(mCanvasBitmap!!)
    }

    //change Canvas to Canvas? if fails
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        canvas.drawBitmap(mCanvasBitmap!!, 0f, 0f, mCanvasPaint)

        for(path in mPaths) {
            mDrawPaint!!.strokeWidth = path.brushThickness
            mDrawPaint!!.color = path.color
            canvas.drawPath(path, mDrawPaint!!)
        }

        if(!mDrawPath!!.isEmpty) {
            mDrawPaint!!.strokeWidth = mDrawPath!!.brushThickness
            mDrawPaint!!.color = mDrawPath!!.color
            canvas.drawPath(mDrawPath!!, mDrawPaint!!)
        }

    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {

        val touchX = event?.x
        val touchY = event?.y

        when(event?.action) {

            MotionEvent.ACTION_DOWN -> {
                mDrawPath!!.color = color
                mDrawPath!!.brushThickness = mBrushSize
                mDrawPath!!.reset()
                mDrawPath!!.moveTo(touchX!!, touchY!!)
            }

            MotionEvent.ACTION_MOVE -> {
                mDrawPath!!.lineTo(touchX!!, touchY!!)
            }

            MotionEvent.ACTION_UP -> {
                mPaths.add(mDrawPath!!)
                mDrawPath = CustomPath(color, mBrushSize)
            }

            else -> return false
        }
        invalidate()
        return true
    }

    fun setSizeForBrush(newSize: Float) {
        mBrushSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, newSize, resources.displayMetrics)
        mDrawPaint!!.strokeWidth=mBrushSize
    }

    fun setColor(newColor: String) {
        color = Color.parseColor(newColor)
        mDrawPaint!!.color=color
    }

    fun undo() {
        if(mPaths.size > 0) {
            mUndoPaths.add(mPaths.removeAt(mPaths.size - 1))
            invalidate()
        }
    }

    fun redo() {
        if(mUndoPaths.size > 0) {
            mPaths.add(mUndoPaths.removeAt(mUndoPaths.size - 1))
            invalidate()
        }
    }

    fun drawArc() {

        val startTop = 0
        val startLeft = 0
        val endBottom = height / 2

        val centerX = width / 2
        val centerY = height / 2

        val upperEdgeX = (centerX + width / 2 * cos(270 * Math.PI / 180)).toInt()
        val upperEdgeY = (centerY + width / 2 * sin(270 * Math.PI / 180)).toInt()

        val bottomEdgeX = (centerX + width / 2 * cos(90 * Math.PI / 180)).toInt()
        val bottomEdgeY = (centerY + width / 2 * sin(90 * Math.PI / 180)).toInt()

        val leftEdgeX = (centerX + width / 2 * cos(180 * Math.PI / 180)).toInt()
        val leftEdgeY = (centerY + width / 2 * sin(180 * Math.PI / 180)).toInt()

        val rightEdgeX = (centerX + width / 2 * cos(0 * Math.PI / 180)).toInt()
        val rightEdgeY = (centerY + width / 2 * sin(0 * Math.PI / 180)).toInt()

        val rect = RectF(startTop.toFloat(), startLeft.toFloat(), endBottom.toFloat(), endBottom.toFloat())

        canvas!!.drawCircle(centerX.toFloat(), centerY.toFloat(), (width / 2).toFloat(), mDrawPaint!!)
        canvas!!.drawCircle(centerX.toFloat(), centerY.toFloat(), (width / 3).toFloat(), mDrawPaint!!) // White circle

    }

    fun drawCircle(centerX: Float, centerY: Float, radius: Float) {
        canvas!!.drawCircle(centerX, centerY, radius, mDrawPaint!!);
        invalidate()
    }

    fun drawRectangle(left: Float, right: Float, top: Float, bottom: Float) {
        // val mpaint = Paint()
        // mpaint.color = Color.RED //set red color for rectangle
        // mpaint.style = Paint.Style.FILL
        // mpaint will fill the rectangle
        canvas!!.drawRect(left, top, right, bottom, mDrawPaint!!)
        invalidate()
    }

    fun drawLine() {
        canvas!!.drawLine(100F, 100F, 480F, 480F, mDrawPaint!!)
        invalidate()
    }

    fun drawOval() {
        var left = 100
        var top = 100
        var right = 600
        var bottom = 400
        canvas!!.drawOval(left.toFloat(), top.toFloat(), right.toFloat(), bottom.toFloat(), mDrawPaint!!)
        invalidate()
    }

    fun clearScreen() {
        mPaths.clear()
        canvas!!.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        invalidate()
    }

    fun setDrawEffects(style: String?) {

        if(style!!.contentEquals("fill")) {
            mDrawPaint!!.style = Paint.Style.FILL
            invalidate()
        }
        else {
            mDrawPaint!!.style = Paint.Style.STROKE
            invalidate()
        }

    }

    internal inner class CustomPath(var color: Int, var brushThickness: Float) : Path() {}
}