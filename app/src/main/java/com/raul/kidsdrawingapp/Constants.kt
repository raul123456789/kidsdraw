package com.raul.kidsdrawingapp

object Constants {
     const val DATABASE_VERSION = 1
     const val DATABASE_NAME = "draw_db"
     const val TABLE_NAME="drawing"
     const val ID_COL="id"
     const val IMAGE_NAME="image_name"
}