package com.raul.kidsdrawingapp

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.provider.MediaStore.Images
import android.text.Editable
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.get
import com.github.dhaval2404.colorpicker.ColorPickerDialog
import com.github.dhaval2404.colorpicker.model.ColorShape
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.circle_coordinates.*
import kotlinx.android.synthetic.main.dialog.view.*
import kotlinx.android.synthetic.main.dialog_brush_size.*
import kotlinx.android.synthetic.main.rectangle_coordinates.*
import kotlinx.android.synthetic.main.select_color_stroke.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class MainActivity : AppCompatActivity() {

    companion object {
        private const val STORAGE_PERMISSION_CODE = 1
        private const val GALLERY = 2
    }

    private  var mImageButtonCurrentPaint: ImageButton?=null
    private  var mSelectValue :Int?=null
    private  var mDrawPaint : Paint? = null

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        drawing_view.setSizeForBrush(10.toFloat())
        drawing_view.setDrawEffects("stroke")

        mImageButtonCurrentPaint = ll_paint_color[3] as ImageButton

        ib_brush.setOnClickListener {
            showBrushSizeDialog()
        }

        mSelectValue=0

        if(mSelectValue==0) {
            ib_fill.setOnClickListener {
                showColorFillDialog()
            }
        }

        ib_gallery.setOnClickListener {
            if(isStorageAllowed()) {
                getImageFromGallery()
            }
            else {
                requestStoragePermission()
            }
        }
        ib_undo.setOnClickListener {
            drawing_view.undo()
        }

        ib_save.setOnClickListener {
            if(isStorageAllowed()) {
                showSaveDialog(fl_drawing_view_container)
            }
            else {
                requestStoragePermission()
            }
        }

        redo.setOnClickListener {
            drawing_view.redo()
            //drawing_view.drawCircle()
        }
    }

    private fun showSaveDialog(flDrawingViewContainer: FrameLayout?) {

        val builder = AlertDialog.Builder(this)

        val dialogView = View.inflate(this, R.layout.dialog, null)

        builder.setView(dialogView)

        val dialog = builder.create()

        dialogView.btn_save.setOnClickListener {
            BitmapAsyncTask(getBitmapFromView(flDrawingViewContainer!!)).execute()
            //saveImageToGallery(this,getBitmapFromView(flDrawingViewContainer!!))
            dialog.dismiss()
        }

        dialogView.btn_cancel.setOnClickListener {
            dialog.dismiss()
        }



        dialog.show()

    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater :MenuInflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }


    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onOptionsItemSelected(item: MenuItem) : Boolean {
        when (item.itemId) {
            R.id.circle -> {
                drawCircleInPosition()
                true
            }
            R.id.rectangle -> {
                drawRectangleInPosition()
                true
            }
            R.id.oval -> {
                drawing_view.drawOval()
                true
            }
            R.id.line -> {
                drawing_view.drawArc()
                true
            }
            R.id.clears -> {
                drawing_view.clearScreen()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
        return true
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun drawRectangleInPosition() {

        val rectanglePositionDialog = Dialog(this)
        rectanglePositionDialog.setContentView(R.layout.rectangle_coordinates)
        rectanglePositionDialog.setTitle("Rectangle Position :")

        val btnSbmit=rectanglePositionDialog.sbmitRect
        var left : Editable? =rectanglePositionDialog.left.text
        var right : Editable? =rectanglePositionDialog.right.text
        var bottom: Editable?=rectanglePositionDialog.bottom.text
        var top:Editable?=rectanglePositionDialog.top.text

        btnSbmit.setOnClickListener {
            drawing_view.drawRectangle(left.toString().toFloat(),
                    right.toString().toFloat(),
                    top.toString().toFloat(),
                    bottom.toString().toFloat())
            rectanglePositionDialog.dismiss()
        }
        rectanglePositionDialog.show()
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun drawCircleInPosition() {

        val circlePositionDialog = Dialog(this)
        circlePositionDialog.setContentView(R.layout.circle_coordinates)
        circlePositionDialog.setTitle("Circle Position :")

        val btnSbmit=circlePositionDialog.sbmitCircle
        var centerX : Editable? =circlePositionDialog.center_x.text
        var centerY : Editable? =circlePositionDialog.center_y.text
        var radius: Editable?=circlePositionDialog.radius.text

        btnSbmit.setOnClickListener {
            if(centerX!!.isNotEmpty() && centerY!!.isNotEmpty() && radius!!.isNotEmpty()) {
                var cenX=centerX.toString().toFloat()
                var cenY=centerY.toString().toFloat()
                var radiusCircle=radius.toString().toFloat()
                drawing_view.drawCircle(cenX, cenY, radiusCircle)
                circlePositionDialog.dismiss()
            }
            else {
                Toast.makeText(this, "Position Coordinates Should not be Empty", Toast.LENGTH_LONG).show()
            }
        }
        circlePositionDialog.show()
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun showColorFillDialog() {

        val colorFillDialog= Dialog(this)
        colorFillDialog.setContentView(R.layout.select_color_stroke)
        colorFillDialog.setTitle("Colour :  ")

        val fill=colorFillDialog.ib_fill_color
        val stroke=colorFillDialog.ib_stroke_color

        fill.setOnClickListener {
            drawing_view.setDrawEffects("fill")
            mSelectValue=1
            ib_fill.setImageResource(R.drawable.fill2)
            colorFillDialog.dismiss()
        }

        stroke.setOnClickListener {
            drawing_view.setDrawEffects("stroke")
            mSelectValue=0
            ib_fill.setImageResource(R.drawable.pencil1)
            colorFillDialog.dismiss()
        }
        colorFillDialog.show()
    }

    private fun getImageFromGallery() {
        val pickPhotoIntent=Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(pickPhotoIntent, GALLERY)
    }

    private fun getBitmapFromView(view: View) : Bitmap {

        val returnedBitmap=Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val canvas= Canvas(returnedBitmap)
        val bgDrawable=view.background
        if(bgDrawable!=null) {
            bgDrawable.draw(canvas)
        }
        else {
            canvas.drawColor(Color.WHITE)
        }
        view.draw(canvas)

        return returnedBitmap
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == STORAGE_PERMISSION_CODE) {
            if(grantResults.isNotEmpty() && grantResults[0] ==
                                PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission Granted For Storage", Toast.LENGTH_LONG).show()
            }
            else {
                Toast.makeText(this, "Permission Denied For Storage", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK) {
            if(requestCode == GALLERY) {
                    try {
                        if(data!!.data!=null) {
                             iv_background.visibility=View.VISIBLE
                             iv_background.setImageURI(data.data)
                        }
                        else{
                            Toast.makeText(this,
                                    "Error Parsing the Image",
                                    Toast.LENGTH_LONG).show()
                        }
                    } catch (e: Exception) {
                        print(e.printStackTrace())
                    }
            }
        }
    }

    private fun requestStoragePermission() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE).toString())) {

            Toast.makeText(this, "Need Permission to add Background", Toast.LENGTH_SHORT).show()

        }
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE),
                STORAGE_PERMISSION_CODE)
    }

    private fun isStorageAllowed():Boolean{
        val result=ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        return result==PackageManager.PERMISSION_GRANTED
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun showBrushSizeDialog() {

        val brushDialog= Dialog(this)
        brushDialog.setContentView(R.layout.dialog_brush_size)
        brushDialog.setTitle("Brush Size:  ")

        val smallBtn=brushDialog.ib_small_brush
        val mediumBtn=brushDialog.ib_medium_brush
        val largeBtn=brushDialog.ib_large_brush
        val extraLargeBtn=brushDialog.ib_extra_large_brush

        smallBtn.setOnClickListener {
            drawing_view.setSizeForBrush(10.toFloat())
            brushDialog.dismiss()
        }

        mediumBtn.setOnClickListener {
            drawing_view.setSizeForBrush(20.toFloat())
            brushDialog.dismiss()
        }

        largeBtn.setOnClickListener {
            drawing_view.setSizeForBrush(30.toFloat())
            brushDialog.dismiss()
        }

        extraLargeBtn.setOnClickListener {
            drawing_view.setSizeForBrush(40.toFloat())
            brushDialog.dismiss()
        }

        brushDialog.show()
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    fun paintClicked(view: View) {
        if(view != mImageButtonCurrentPaint) {
             val imageButton = view as ImageButton
             val colorTag=imageButton.tag.toString()
             drawing_view.setColor(colorTag)
             imageButton.setImageDrawable(
                     ContextCompat.getDrawable(this, R.drawable.pallet_pressed)
             )
            mImageButtonCurrentPaint!!.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.pallet_normal))
            mImageButtonCurrentPaint=view
        }
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    @SuppressLint("ResourceType")
    fun colourPicker(view: View) {
        ColorPickerDialog
            .Builder(this) // Pass Activity Instance
            .setColorShape(ColorShape.CIRCLE) // Or ColorShape.CIRCLE
            .setDefaultColor(Color.RED) // Pass Default Color
            .setColorListener { color, colorHex ->
               drawing_view.setColor(colorHex)
            }
            .setDismissListener {
                Log.d("ColorPickerDialog", "Handle dismiss event")
            }
            .show()
    }

    private inner class BitmapAsyncTask(val mBitmap: Bitmap) :AsyncTask<Any, Void, String>() {

        private lateinit var mProgressDialog : Dialog

        override fun onPreExecute() {
            super.onPreExecute()
            showProgressDialog()
        }

        override fun doInBackground(vararg params: Any?): String {
            var result = ""
            if(mBitmap!=null) {
                try {
                    val bytes = ByteArrayOutputStream()
                    mBitmap.compress(Bitmap.CompressFormat.PNG, 90, bytes)

                    Images.Media.insertImage(contentResolver, mBitmap, "KidsDrawing" , "");

                    val file = File(externalCacheDir!!
                            .absoluteFile.toString()
                            + File.separator
                            + "KidsDrawingApp_"
                            + System.currentTimeMillis() / 1000 + ".png")

                    val fo = FileOutputStream(file)
                    fo.write(bytes.toByteArray())
                    fo.close()
                    result=file.absolutePath

                } catch (e: Exception) {
                    result=""
                    e.printStackTrace()
                }
            }
            return result
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            if(result!!.isNotEmpty()) {
                cancelProgressDialog()
                Toast.makeText(this@MainActivity, "Files Saved Successfully", Toast.LENGTH_SHORT).show()
            }
            else {
                cancelProgressDialog()
                Toast.makeText(this@MainActivity, "Something went wrong while saving the file", Toast.LENGTH_SHORT).show()
            }
        }

        private fun showProgressDialog() {
            mProgressDialog=Dialog(this@MainActivity)
            mProgressDialog.setContentView(R.layout.dialog_custom_progress)
            mProgressDialog.show()
        }

        private fun cancelProgressDialog() {
            mProgressDialog.dismiss()
        }

    }



}